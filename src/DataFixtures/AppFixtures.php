<?php

namespace App\DataFixtures;

use App\Entity\Classe;
use App\Entity\Enseignant;
use App\Entity\Eleve;
use App\Entity\Jeu;
use App\Entity\Document;
use App\Entity\Message;
use App\Entity\Utilisateur;
use App\Entity\Video;
use App\Entity\Calendrier;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        // Classe
        $classe1 = new Classe();
        $classe1->setId(1)
            ->setNom("Petite section");
        $manager->persist($classe1);

        $classe2 = new Classe();
        $classe2->setId(2)
            ->setNom("Moyenne section");
        $manager->persist($classe2);

        $classe3 = new Classe();
        $classe3->setId(3)
            ->setNom("Grande section");
        $manager->persist($classe3);

        // Enseignant
        $enseignant1 = new Enseignant();
        $enseignant1->setId(11)
            ->setNom("Hubert")
            ->setPrenom("Jean")
            ->setClasse($classe1)
            ->setMail("jean.hubert@gmail.com");
        $manager->persist($enseignant1);

        $enseignant2 = new Enseignant();
        $enseignant2->setId(12)
            ->setNom("Dupont")
            ->setPrenom("Margaux")
            ->setClasse($classe2)
            ->setMail("margaux.dupont@gmail.com");
        $manager->persist($enseignant2);

        $enseignant3 = new Enseignant();
        $enseignant3->setId(13)
            ->setNom("Garcia")
            ->setPrenom("Michelle")
            ->setClasse($classe3)
            ->setMail("garcia.michelle@gmail.com");
        $manager->persist($enseignant3);

        // Eleve
        $eleve1 = new Eleve();
        $eleve1->setId(21)
            ->setNom("Bonnet")
            ->setPrenom("Jules")
            ->setMail("jules.bonnet@gmail.com")
            ->setClasse($classe1);
        $manager->persist($eleve1);

        $eleve2 = new Eleve();
        $eleve2->setId(22)
            ->setNom("Vernier")
            ->setPrenom("Mathilde")
            ->setMail("mathilde.vernier@gmail.com")
            ->setClasse($classe1);
        $manager->persist($eleve2);

        $eleve3 = new Eleve();
        $eleve3->setId(23)
            ->setNom("Lavigne")
            ->setPrenom("Florent")
            ->setMail("florent.lavigne@gmail.com")
            ->setClasse($classe1);
        $manager->persist($eleve3);

        $eleve4 = new Eleve();
        $eleve4->setId(24)
            ->setNom("Gachet")
            ->setPrenom("Lisa")
            ->setMail("lisa.gachet@gmail.com")
            ->setClasse($classe1);
        $manager->persist($eleve4);

        $eleve5 = new Eleve();
        $eleve5->setId(25)
            ->setNom("Cailloux")
            ->setPrenom("Bastien")
            ->setMail("bastien.cailloux@gmail.com")
            ->setClasse($classe2);
        $manager->persist($eleve5);

        $eleve6 = new Eleve();
        $eleve6->setId(26)
            ->setNom("Larousse")
            ->setPrenom("Elise")
            ->setMail("elise.larousse@gmail.com")
            ->setClasse($classe2);
        $manager->persist($eleve6);

        $eleve7 = new Eleve();
        $eleve7->setId(27)
            ->setNom("Duval")
            ->setPrenom("Bruno")
            ->setMail("bruno.duval@gmail.com")
            ->setClasse($classe2);
        $manager->persist($eleve7);

        $eleve8 = new Eleve();
        $eleve8->setId(28)
            ->setNom("Leclère")
            ->setPrenom("Julie")
            ->setMail("julie.leclere@gmail.com")
            ->setClasse($classe2);
        $manager->persist($eleve8);

        $eleve9 = new Eleve();
        $eleve9->setId(29)
            ->setNom("Chapelle")
            ->setPrenom("Théo")
            ->setMail("theo.chapelle@gmail.com")
            ->setClasse($classe3);
        $manager->persist($eleve9);

        $eleve10 = new Eleve();
        $eleve10->setId(210)
            ->setNom("Popelin")
            ->setPrenom("Sophie")
            ->setMail("sophie.popelin@gmail.com")
            ->setClasse($classe3);
        $manager->persist($eleve10);

        $eleve11 = new Eleve();
        $eleve11->setId(211)
            ->setNom("Venturion")
            ->setPrenom("Marie")
            ->setMail("marie.venturion@gmail.com")
            ->setClasse($classe3);
        $manager->persist($eleve11);

        $eleve12 = new Eleve();
        $eleve12->setId(212)
            ->setNom("Busson")
            ->setPrenom("Alexandre")
            ->setMail("alexandre.busson@gmail.com")
            ->setClasse($classe3);
        $manager->persist($eleve12);

        //Jeu
        $jeu1 = new Jeu();
        $jeu1->setNom("Reconnaissance couleur")
            ->setDescription("Essaye de reconnaître les couleurs")
            ->setLien("http://jeu-couleur.fr");
        $manager->persist($jeu1);

        $jeu2 = new Jeu();
        $jeu2->setNom("Reconnaissance d'animaux")
            ->setDescription("Essaye de reconnaître les animaux")
            ->setLien("http://jeu-animal.fr");
        $manager->persist($jeu2);

        $jeu3 = new Jeu();
        $jeu3->setNom("Dessin avec modèle")
            ->setDescription("Recopie le modèle")
            ->setLien("http://jeu-modele.fr");
        $manager->persist($jeu3);

        //Document
        $document1 = new Document();
        $document1->setNom("Document 1")
            ->setDescription("Description du document 1")
            ->setLien("http://document1.fr");
        $manager->persist($document1);

        $document2 = new Document();
        $document2->setNom("Document 2")
            ->setDescription("Description du document 2")
            ->setLien("http://document2.fr");
        $manager->persist($document2);

        //Utilisateur
        $utilisateur1 = new Utilisateur();
        $utilisateur1->setIdentifiant("admin")
            ->setIdCible(30)
            ->setRoles(["ROLE_ADMIN"])
            ->setPassword('$argon2id$v=19$m=65536,t=4,p=1$KgArujuhnhp0BjJHqLhCNA$UTJ1Kg3uqftAigOL/Z1QJyzSoj+EfLdy6TtC/aY7nrw'); // admin123
        $manager->persist($utilisateur1);

        $utilisateur2 = new Utilisateur();
        $utilisateur2->setIdentifiant("jean.hubert")
            ->setIdCible(11)
            ->setRoles(["ROLE_ENSEIGNANT"])
            ->setPassword('$argon2id$v=19$m=65536,t=4,p=1$rUyIU0PRlE97zsXhCSgWlg$Kr8M/ZYviNbBDSPf/hdwx0XX5uHcQMX1Kyw9jWHEsPE'); // abc123
        $manager->persist($utilisateur2);

        $utilisateur3 = new Utilisateur();
        $utilisateur3->setIdentifiant("elise.larousse")
            ->setIdCible(26)
            ->setRoles(["ROLE_ELEVE"])
            ->setPassword('$argon2id$v=19$m=65536,t=4,p=1$kwfMWkbdBhQpceYbsjMNmw$ttVgyjN7TI9P10XiF7UQ5xIEcWyH8X/7ltTGHsHX28s'); // abc123
        $manager->persist($utilisateur3);

        $utilisateur4 = new Utilisateur();
        $utilisateur4->setIdentifiant("margaux.dupont")
            ->setIdCible(12)
            ->setRoles(["ROLE_ENSEIGNANT"])
            ->setPassword('$argon2id$v=19$m=65536,t=4,p=1$YwmOSTdf/Lk19qkKWRbnfw$oDDUffbXLpzUPXLjFfqbrPnfHX20OJ9XtXN40n/EtXo'); // abc123
        $manager->persist($utilisateur4);

        $utilisateur5 = new Utilisateur();
        $utilisateur5->setIdentifiant("michelle.garcia")
            ->setIdCible(13)
            ->setRoles(["ROLE_ENSEIGNANT"])
            ->setPassword('$argon2id$v=19$m=65536,t=4,p=1$72JlbP1CiVPMClW3WItLIA$zIQdxQqA3RTZBlkjpG0OpFSNi2baUQswc59AVSWNnSk'); // abc123
        $manager->persist($utilisateur5);

        $utilisateur6 = new Utilisateur();
        $utilisateur6->setIdentifiant("jules.bonnet")
            ->setIdCible(21)
            ->setRoles(["ROLE_ELEVE"])
            ->setPassword('$argon2id$v=19$m=65536,t=4,p=1$V/WAvz5tnrvefMewHutxYg$rMLkXm5jx6HHuGRti4TfB+/Hy10bGX383BoF6sUXVhA'); // abc123
        $manager->persist($utilisateur6);

        $utilisateur7 = new Utilisateur();
        $utilisateur7->setIdentifiant("sophie.popelin")
            ->setIdCible(210)
            ->setRoles(["ROLE_ELEVE"])
            ->setPassword('$argon2id$v=19$m=65536,t=4,p=1$eiqQyaI7V7pGqUJbUZwOzQ$fKLr5OM6j3LhKcEWZIIj0O4KwNWo3tyeAfaVWacrGGo'); // abc123
        $manager->persist($utilisateur7);


        //Message
        $message1 = new Message();
        $message1->setMessage("Correction de l'exercice demain")
            ->setDate("10/09/2020")
            ->setUtilisateur1($utilisateur2)
            ->setUtilisateur2($utilisateur3)
            ->setLu(false);
        $manager->persist($message1);

        $message2 = new Message();
        $message2->setMessage("Bien reçu")
            ->setDate("12/09/2020")
            ->setUtilisateur1($utilisateur3)
            ->setUtilisateur2($utilisateur2)
            ->setLu(false);
        $manager->persist($message2);


        //Vidéo
        $video1 = new Video();
        $video1->setNom("La danse des haricots")
            ->setDescription("Il était une fois un grand potager")
            ->setLien("http://haricot-danse.fr");
        $manager->persist($video1);

        $video2 = new Video();
        $video2->setNom("L'histoire des oursons")
            ->setDescription("La grande histoires des oursons !")
            ->setLien("http://histoire-ourson.fr");
        $manager->persist($video2);

        //Calendrier
        $calendrier1 = new Calendrier();
        $calendrier1->setAnnee(2020)
            ->setMois(11)
            ->setJours(24)
            ->setDescription("Ecrire l'alphabet 4 fois dans l'ordre")
            ->setClasse($classe1)
            ->setSemaine(1);
        $manager->persist($calendrier1);

        $calendrier2 = new Calendrier();
        $calendrier2->setAnnee(2021)
            ->setMois(5)
            ->setJours(19)
            ->setDescription("Reciter le poeme 3 fois + écrire les nombres jusqu'a 200 en lettre + regarder la video")
            ->setClasse($classe1)
            ->addVideo($video1)
            ->setSemaine(2);
        $manager->persist($calendrier2);

        $calendrier3 = new Calendrier();
        $calendrier3->setAnnee(2021)
            ->setMois(6)
            ->setJours(3)
            ->setDescription("Dites oui 30 fois")
            ->setClasse($classe2)
            ->addVideo($video1)
            ->setSemaine(2);
        $manager->persist($calendrier3);


        $manager->flush();
    }
}
