<?php

namespace App\Form;

use App\Entity\Utilisateur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Positive;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('identifiant')

            ->add('nomEleve', TextType::class, [
                'mapped' => false,
                'required' => false,
            ])
            ->add('prenomEleve', TextType::class, [
                'mapped' => false,
                'required' => false,
            ])
            ->add('mailEleve', TextType::class, [
                'mapped' => false,
                'required' => false,
            ])

            ->add('nomEnseignant', TextType::class, [
                'mapped' => false,
                'required' => false,
            ])
            ->add('prenomEnseignant', TextType::class, [
                'mapped' => false,
                'required' => false,
            ])
            ->add('mailEnseignant', TextType::class, [
                'mapped' => false,
                'required' => false,
            ])

            ->add('eleve', CheckboxType::class, [
                'mapped' => false,
                'required' => true,
                'attr' => ['onclick' => 'newEleve()', 'style' => 'opacity: 1; margin-top: 5px; margin-left: 10px;'],
            ])
            ->add('enseignant', CheckboxType::class, [
                'mapped' => false,
                'required' => true,
                'attr' => ['onclick' => 'newEnseignant()', 'style' => 'opacity: 1; margin-top: 5px; margin-left: 10px;'],
            ])
            ->add('admin', CheckboxType::class, [
                'mapped' => false,
                'required' => true,
                'attr' => ['onclick' => 'testAdmin()', 'style' => 'opacity: 1; margin-top: 5px; margin-left: 10px;'],
            ])
            ->add('classeEl', ChoiceType::class, [
                'choices' => [
                    'Petite section' => 1,
                    'Moyenne section' => 2,
                    'Grande section' => 3
                ],
                'mapped' => false,
                'required' => false,
                'attr' => ['style' => 'display: block'],
            ])
            ->add('classeEn', ChoiceType::class, [
                'choices' => [
                    'Sans classe' => 0,
                    'Petite section' => 1,
                    'Moyenne section' => 2,
                    'Grande section' => 3
                ],
                'mapped' => false,
                'required' => false,
                'attr' => ['style' => 'display: block'],
            ])
            ->add('plainPassword', PasswordType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez entrer un mot de passe',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Votre mot de passe doit contenir au moins {{ limit }} caratères',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Utilisateur::class,
        ]);
    }
}
