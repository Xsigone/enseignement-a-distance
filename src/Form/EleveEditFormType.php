<?php

namespace App\Form;

use App\Entity\Utilisateur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Positive;

class EleveEditFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('identifiant')

            ->add('nom', TextType::class, [
                'mapped' => false,
                'required' => true,
            ])
            ->add('prenom', TextType::class, [
                'mapped' => false,
                'required' => true,
            ])
            ->add('mail', TextType::class, [
                'mapped' => false,
                'required' => true,
            ])

            ->add('classe', ChoiceType::class, [
                'choices' => [
                    'Petite section' => 1,
                    'Moyenne section' => 2,
                    'Grande section' => 3
                ],
                'mapped' => false,
                'required' => true,
                'attr' => ['style' => 'display: block'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Utilisateur::class,
        ]);
    }
}
