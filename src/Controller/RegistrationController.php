<?php

namespace App\Controller;

use App\Entity\Classe;
use App\Entity\Eleve;
use App\Entity\Enseignant;
use App\Entity\Utilisateur;
use App\Form\RegistrationFormType;
use App\Repository\ClasseRepository;
use App\Repository\EleveRepository;
use App\Repository\EnseignantRepository;
use App\Security\AppCustomAuthenticator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, ClasseRepository $classeRepository, EleveRepository $eleveRepository, EnseignantRepository $enseignantRepository): Response
    {
        if ($this->getUser() and in_array('ROLE_ADMIN', $this->getUser()->getRoles())) {
            $user = new Utilisateur();
            $form = $this->createForm(RegistrationFormType::class, $user);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $changementEns = false;
                // encode the plain password
                $entityManager = $this->getDoctrine()->getManager();
                $user->setPassword(
                    $passwordEncoder->encodePassword(
                        $user,
                        $form->get('plainPassword')->getData()
                    )
                );

                $roles = [];
                // Si c'est une eleve
                if($form->get('eleve')->getData()){
                    $roles[] = "ROLE_ELEVE";
                    $eleve = new Eleve();

                    // Mise en place de l'id
                    $eleves = $eleveRepository->findAll();
                    $idMax = null;
                    foreach ($eleves as $e){
                        if ($idMax==null or $e->getId()>$idMax){
                            $idMax = $e->getId();
                        }
                    }
                    $tempId = (($idMax - pow(10, strlen((string)$idMax) - 1) * 2) + 1);
                    $id = $tempId + pow(10, strlen((string)$tempId)) * 2;

                    // Recuperation de la classe
                    // Mise en place des informations
                    $eleve->setId($id)
                        ->setPrenom($form->get('prenomEleve')->getData())
                        ->setNom($form->get('nomEleve')->getData())
                        ->setMail($form->get('mailEleve')->getData())
                        ->setClasse($classeRepository->find($form->get('classeEl')->getData()));
                    $user->setIdCible($id);
                    $entityManager->persist($eleve);

                }

                // Si c'est une enseignant
                if($form->get('enseignant')->getData()){
                    $roles[] = "ROLE_ENSEIGNANT";
                    $enseignant = new Enseignant();

                    // Mise en place de l'id
                    $enseignants = $enseignantRepository->findAll();
                    $idMax = null;
                    foreach ($enseignants as $e){
                        if ($idMax==null or $e->getId()>$idMax){
                            $idMax = $e->getId();
                        }
                    }
                    $tempId = (($idMax - pow(10, strlen((string)$idMax) - 1)) + 1);
                    $id = $tempId + pow(10, strlen((string)$tempId));

                    // Mise en place des informations
                    $idClasse = $form->get('classeEn')->getData();
                    $enseignant->setId($id)
                        ->setPrenom($form->get('prenomEnseignant')->getData())
                        ->setNom($form->get('nomEnseignant')->getData())
                        ->setMail($form->get('mailEnseignant')->getData());
                    $user->setIdCible($id);
                    $classe = $classeRepository->find($idClasse);
                    if ($idClasse!=0 and $classe->getEnseignant()==null) {
                        $enseignant->setClasse($classe);
                    } elseif ($idClasse!=0){
                        $changementEns = true;
                    }
                    $entityManager->persist($enseignant);

                }

                // Si c'est une admin
                if($form->get('admin')->getData()){
                    $roles[] = "ROLE_ADMIN";
                }
                $user->setRoles($roles);

                $entityManager->persist($user);
                $entityManager->flush();
                // do anything else you need here, like send an email

                if ($changementEns){
                    return $this->redirectToRoute('changement_classe', ['id' => $enseignant->getId(), 'idClasse' => $idClasse]);
                }

                return $this->redirectToRoute('accueil');
            }
            return $this->render('registration/register.html.twig', [
                'registrationForm' => $form->createView(),
                'classes' => $classeRepository->findAll(),
            ]);
        } else {
            return $this->redirectToRoute('accueil');
        }
    }

    /**
     * @Route("/register/classe/{id}/changement/{idClasse}", name="changement_classe", requirements={"idClasse":"\d+", "id":"\d+"})
     */
    public function changementClasse(Enseignant $enseignant, int $idClasse, ClasseRepository $classeRepository): Response
    {
        if (!$this->getUser() or !in_array('ROLE_ADMIN', $this->getUser()->getRoles())){
            return $this->redirectToRoute("accueil");
        }
        return $this->render('changementClasse.html.twig', [
            'classe' => $classeRepository->find($idClasse),
            'enseignant' => $enseignant,
        ]);

    }

    /**
     * @Route("/register/classe/{id}/changement/{idClasse}/valide", name="valider_changement_classe", requirements={"idClasse":"\d+", "id":"\d+"})
     */
    public function changementClasseValide(Enseignant $enseignant, int $idClasse, ClasseRepository $classeRepository): Response
    {
        if (!$this->getUser() or !in_array('ROLE_ADMIN', $this->getUser()->getRoles())){
            return $this->redirectToRoute("accueil");
        }
        $classe = $classeRepository->find($idClasse);
        $ancienEns = $classe->getEnseignant();

        $ancienEns->setClasse(null);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($ancienEns);
        $entityManager->flush();

        if ($enseignant->getClasse()){
            $ancienClasse = $enseignant->getClasse();
            $ancienClasse->setEnseignant(null);
            $entityManager->persist($ancienClasse);
        }

        $classe->setEnseignant($enseignant);
        $enseignant->setClasse($classe);
        $entityManager->persist($classe);
        $entityManager->persist($enseignant);
        $entityManager->flush();
        return $this->redirectToRoute('info_enseignant', ['id'=>$enseignant->getId()]);

    }
}
