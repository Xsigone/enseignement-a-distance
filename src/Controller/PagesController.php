<?php

namespace App\Controller;

use App\Entity\Calendrier;
use App\Entity\Classe;
use App\Entity\Document;
use App\Entity\Eleve;
use App\Entity\Enseignant;
use App\Entity\Jeu;
use App\Entity\Message;
use App\Entity\Utilisateur;
use App\Entity\Video;
use App\Form\CalendrierFormType;
use App\Form\DocumentFormType;
use App\Form\EleveEditFormType;
use App\Form\EnseignantEditFormType;
use App\Form\JeuFormType;
use App\Form\MessageFormType;
use App\Form\RegistrationFormType;
use App\Form\VideoFormType;
use App\Repository\CalendrierRepository;
use App\Repository\ClasseRepository;
use App\Repository\EleveRepository;
use App\Repository\EnseignantRepository;
use App\Repository\MessageRepository;
use App\Repository\UtilisateurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use function Sodium\add;

class PagesController extends AbstractController
{
    /**
     * @Route("/", name="accueil")
     */
    public function index(CalendrierRepository $calendrierRepository, EleveRepository $eleveRepository, EnseignantRepository $enseignantRepository): Response
    {
        $user = $this->getUser();
        if ($user and in_array('ROLE_ELEVE', $user->getRoles())){
            $eleve = $eleveRepository->find($user->getIdCible());
            $jour = date('d');
            $mois = date('m');
            $annee = date('Y');
            $devoir = true;
            $calendrier = $calendrierRepository->findCalendrierJourByClasse($jour, $mois, $annee, $eleve->getClasse()->getId());
            if ($calendrier == null){
                $devoir = false;
            }
            $msgs = $this->getUser()->getMessagesRecv();
            $newMsg = [];
            foreach ($msgs as $msg){
                if (!$msg->getLu()){
                    $newMsg[] = $msg;
                }
            }
            return $this->render('indexEleve.html.twig', [
                'controller_name' => 'Accueil Eleve',
                'eleve' => $eleve,
                'calendrier' => $calendrier,
                'devoir' => $devoir,
                'msgs' => $newMsg,
            ]);
        } elseif ($user and in_array('ROLE_ENSEIGNANT', $user->getRoles())){
            $enseignant = $enseignantRepository->find($user->getIdCible());
            $msgs = $this->getUser()->getMessagesRecv();
            $newMsg = [];
            foreach ($msgs as $msg){
                if (!$msg->getLu()){
                    $newMsg[] = $msg;
                }
            }
            return $this->render('indexEnseignant.html.twig', [
                'controller_name' => 'Accueil Enseignant',
                'enseignant' => $enseignant,
                'msgs' => $newMsg,
            ]);
        } elseif ($user and in_array('ROLE_ADMIN', $user->getRoles())){
            return $this->redirectToRoute('liste_utilisateur');
        } else {
            return $this->render('pages/index.html.twig', [
                'controller_name' => 'Accueil',
            ]);
        }
    }

    /**
     * @Route("/calendrier", name="calendrier_now", requirements={"id":"\d+"})
     */
    public function calendrierNow(EnseignantRepository $enseignantRepository): Response
    {
        if (!$this->getUser() or (in_array('ROLE_ENSEIGNANT', $this->getUser()->getRoles()) and $enseignantRepository->find($this->getUser()->getIdCible())->getClasse()==null)){
            return $this->redirectToRoute("accueil");
        }
        $jour = date('d');
        $mois = date('m');
        $annee = date('Y');
        $nsemaine = date('W');
        return $this->redirectToRoute('calendrier', ['annee' => $annee, 'mois' => $mois, 'nsemaine' => $nsemaine, 'jour' => $jour]);
    }

    /**
     * @Route("/calendrier/{annee}/{mois}/{nsemaine}/{jour}", name="calendrier", requirements={"annee":"\d+", "mois":"\d+", "semaine":"\d+", "jour":"\d+"})
     */
    public function calendrier(CalendrierRepository $calendrierRepository, EleveRepository $eleveRepository, EnseignantRepository $enseignantRepository, int $annee, int $mois, int $nsemaine, int $jour): Response
    {
        $user = $this->getUser();
        if (!$this->getUser() or (in_array('ROLE_ENSEIGNANT', $this->getUser()->getRoles()) and $enseignantRepository->find($this->getUser()->getIdCible())->getClasse()==null)){
            return $this->redirectToRoute("accueil");
        }
        $joursemaine = date('N', strtotime($annee."-".$mois."-".$jour));
        $classe = null;
        if (in_array('ROLE_ELEVE', $user->getRoles())){
            $classe = $eleveRepository->find($user->getIdCible())->getClasse();
        } elseif (in_array('ROLE_ENSEIGNANT', $user->getRoles())){
            $classe = $enseignantRepository->find($user->getIdCible())->getClasse();
        }
        if ($user and (in_array('ROLE_ELEVE', $user->getRoles()) or in_array('ROLE_ENSEIGNANT', $user->getRoles()))){
            //$jour = date('d');
            //$mois = date('m');
            //$annee = date('Y');
            //$joursemaine = date('N');
            //$nsemaine = date('W');

            $semaine = [];
            $tempsemaine = [];
            $i = 1;
            while ($i<=7){
                $calen = new Calendrier();
                $calen->setSemaine($nsemaine)
                    ->setClasse($classe)
                    ->setDescription("Rien à faire pour aujourd'hui");

                $jours = $jour - ($joursemaine-$i);
                if ($jours>date('t', strtotime($annee."-".$mois."-01"))){ // Si le jour est superieurs au nombre de jours dans le mois
                    $calen->setJours($jours-date('t', strtotime($annee."-".$mois."-01")));
                    if ($mois==12){ // Si le mois est Décembre
                        $calen->setMois(1) // On change d'annee
                            ->setAnnee($annee+1);
                    } else { // Sinon on passe au mois prochain
                        $calen->setMois($mois+1)
                            ->setAnnee($annee);
                    }
                } elseif ($jours<=0){ // Si le jour est inférieur ou egale à 0
                    if ($mois==1){ // Si le mois est Janvier
                        $tempAnn = $annee-1; // On change d'annee
                        $calen->setJours(date('t', strtotime($tempAnn."-12-01"))-$jours)
                            ->setMois(12)
                            ->setAnnee($tempAnn);
                    } else {
                        $tempMois = $mois-1; // Sinon on passe au mois précedent
                        $calen->setJours(date('t', strtotime($annee."-".$tempMois."-01"))+$jours)
                            ->setMois($tempMois)
                            ->setAnnee($annee);
                    }
                } else {
                    $calen->setJours($jours)
                        ->setMois($mois)
                        ->setAnnee($annee);
                }
                $tempsemaine[] = $calen;
                $i++;
            }

            foreach ($tempsemaine as $jours){
                $calendrier = $calendrierRepository->findCalendrierJourByClasse($jours->getJours(), $jours->getMois(), $jours->getAnnee(), $classe);
                if ($calendrier!=null){
                    $semaine[] = $calendrier;
                } else {
                    $semaine[] = $jours;
                }
            }

            $jourprec = new Calendrier();
            $jourprec->setClasse($semaine[0]->getClasse())
                ->setSemaine($semaine[0]->getSemaine()-1);

            if ($semaine[0]->getJours()==1) {
                if ($semaine[0]->getMois() == 1) { // Si le mois est Janvier
                    $tempAnn = $semaine[0]->getAnnee() - 1; // On change d'annee
                    $jourprec->setJours(date('t', strtotime($tempAnn . "-12-01")))
                        ->setMois(12)
                        ->setAnnee($tempAnn);
                } else {
                    $tempMois = $semaine[0]->getMois()-1; // Sinon on passe au mois précedent
                    $jourprec->setJours(date('t', strtotime($annee."-".$tempMois."-01")))
                        ->setMois($tempMois)
                        ->setAnnee($annee);
                }
            } else {
                $jourprec->setJours($semaine[0]->getJours()-1)
                    ->setMois($semaine[0]->getMois())
                    ->setAnnee($semaine[0]->getAnnee());
            }

            $joursuiv = new Calendrier();
            $joursuiv->setClasse($semaine[6]->getClasse())
                ->setSemaine($semaine[6]->getSemaine()+1);

            if ($semaine[6]->getJours()==date('t', strtotime($semaine[6]->getAnnee()."-".$semaine[6]->getMois()."-01"))) { // Si le jour est superieurs au nombre de jours dans le mois
                $joursuiv->setJours(1);
                if ($semaine[6]->getMois() == 12) { // Si le mois est Décembre
                    $joursuiv->setMois(1) // On change d'annee
                    ->setAnnee($semaine[6]->getAnnee() + 1);
                } else { // Sinon on passe au mois prochain
                    $joursuiv->setMois($semaine[6]->getMois() + 1)
                        ->setAnnee($semaine[6]->getAnnee());
                }
            } else {
                $joursuiv->setJours($semaine[6]->getJours()+1)
                    ->setMois($semaine[6]->getMois())
                    ->setAnnee($semaine[6]->getAnnee());
            }

            return $this->render('calendrier.html.twig', [
                'controller_name' => 'Calendrier',
                'semaine' => $semaine,
                'jourprec' => $jourprec,
                'joursuiv' => $joursuiv,
            ]);
        } else {
            return $this->redirectToRoute('accueil');
        }
    }

    /**
     * @Route("/messagerie", name="messagerie")
     */
    public function messagerie(): Response
    {
        $user = $this->getUser();
        if ($user){
            $msgsS = $user->getMessagesSend();
            $msgsR = $user->getMessagesRecv();
            $listeContact = [];
            foreach ($msgsS as $msg){
                $uti = $msg->getUtilisateur2();
                if (!in_array($uti, $listeContact)){
                    $listeContact[] = $uti;
                }
            }
            foreach ($msgsR as $msg){
                $uti = $msg->getUtilisateur1();
                if (!in_array($uti, $listeContact)){
                    $listeContact[] = $uti;
                }
            }
            return $this->render('messagerie.html.twig', [
                'controller_name' => 'Messagerie',
                'liste_contact' => $listeContact,
            ]);
        } else {
            return $this->redirectToRoute('accueil');
        }
    }

    /**
     * @Route("/message/{id}/lu", name="message_lu", methods={"GET","POST"}, requirements={"id":"\d+"})
     */
    public function lu(Message $message): Response
    {
        if ($this->getUser()) {
            if ($message->getUtilisateur2()->getId() == $this->getUser()->getId()) {
                $entityManager = $this->getDoctrine()->getManager();
                $message->setLu(true);
                $entityManager->persist($message);
                $entityManager->flush();
            }
            return $this->redirectToRoute('accueil');
        } else {
            return $this->redirectToRoute("accueil");
        }
    }


    /**
     * @Route("/messagerie/contacts", name="liste_contact", methods={"GET","POST"})
     */
    public function liste_contact(UtilisateurRepository $utilisateurRepository): Response
    {
        if ($this->getUser()) {
            $contacts = $utilisateurRepository->findAll();

            return $this->render('listeContact.html.twig', [
                'controller_name' => 'Liste contact',
                'utilisateur' => $utilisateurRepository->find($this->getUser()->getId()),
                'liste_contact' => $contacts,
            ]);
        } else {
            return $this->redirectToRoute("accueil");
        }
    }

    /**
     * @Route("/message/{id}", name="message", methods={"GET","POST"}, requirements={"id":"\d+"})
     */
    public function message(Request $request, Utilisateur $utilisateur, UtilisateurRepository $utilisateurRepository, MessageRepository $messageRepository): Response
    {
        if ($this->getUser()) {
            $jour = date('d');
            $mois = date('m');
            $annee = date('Y');
            $user = $utilisateurRepository->find($this->getUser()->getId());
            $message = new Message();
            $message->setUtilisateur1($user);
            $message->setUtilisateur2($utilisateur);
            $message->setLu(false);
            $message->setDate($jour."/".$mois."/".$annee);
            $form = $this->createForm(MessageFormType::class, $message);
            $form->handleRequest($request);

            $messages = $messageRepository->findByUser($user, $utilisateur);

            if ($form->isSubmitted() && $form->isValid()) {
                // encode the plain password

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($message);
                $entityManager->flush();
                // do anything else you need here, like send an email

                return $this->redirectToRoute('message', ['id' => $utilisateur->getId()]);
            }

            return $this->render('message.html.twig', [
                'messageForm' => $form->createView(),
                'utilisateur' => $utilisateur,
                'messages' => $messages,
            ]);
        } else {
            return $this->redirectToRoute("accueil");
        }
    }


    /**
     * @Route("/eleve/infos/{id}", name="info_eleve", requirements={"id":"\d+"})
     */
    public function infosEleve(Eleve $eleve, UtilisateurRepository $utilisateurRepository) : Response
    {
        if ($this->getUser()) {
            return $this->render("infosEleve.html.twig", [
                'controller_name'=>'profil eleve',
                'eleve' => $eleve,
                'title' => 'mon profil',
                'uti' => $utilisateurRepository->findbyIdCible($eleve->getId()),
            ]);
        } else {
            return $this->redirectToRoute("accueil");
        }
    }

    /**
     * @Route("/enseignant/infos/{id}", name="info_enseignant", requirements={"id":"\d+"})
     */
    public function infosEnseignant(Enseignant $enseignant, UtilisateurRepository $utilisateurRepository) : Response
    {
        if ($this->getUser()) {
            return $this->render("infosEnseignant.html.twig", [
                'controller_name' => 'profil enseignant',
                'enseignant' => $enseignant,
                'title' => 'mon profil',
                'uti' => $utilisateurRepository->findbyIdCible($enseignant->getId()),
            ]);
        } else {
            return $this->redirectToRoute("accueil");
        }

    }

    /**
     * @Route("/calendrier/{annee}/{mois}/{nsemaine}/{jour}/new", name="calendrier_new", methods={"GET","POST"}, requirements={"annee":"\d+", "mois":"\d+", "semaine":"\d+", "jour":"\d+"})
     */
    public function calendrierNew(Request $request, int $annee, int $mois, int $nsemaine, int $jour, EnseignantRepository $enseignantRepository, CalendrierRepository $calendrierRepository): Response
    {
        if ($this->getUser() and in_array("ROLE_ENSEIGNANT", $this->getUser()->getRoles())) {
            $classe = $enseignantRepository->find($this->getUser()->getIdCible())->getClasse();
            $tempcal = $calendrierRepository->findCalendrierJourByClasse($jour, $mois, $annee, $classe);
            if ($tempcal == null) {
                $calendrier = new Calendrier();
                $calendrier->setAnnee($annee)
                    ->setMois($mois)
                    ->setJours($jour)
                    ->setSemaine($nsemaine)
                    ->setClasse($classe);
            } else {
                $calendrier = $tempcal;
            }

            $form = $this->createForm(CalendrierFormType::class, $calendrier);
            $form->handleRequest($request);


            if ($form->isSubmitted() && $form->isValid()) {
                // encode the plain password

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($calendrier);
                $entityManager->flush();
                // do anything else you need here, like send an email

                return $this->redirectToRoute('calendrier', ['annee' => $annee, 'mois' => $mois, 'nsemaine' => $nsemaine, 'jour' => $jour]);
            }

            return $this->render('calendrierNew.html.twig', [
                'calendrierForm' => $form->createView(),
                'calendrier' => $calendrier,
            ]);
        }else{
            return $this->redirectToRoute('accueil');
        }

    }

    /**
     * @Route("/calendrier/{annee}/{mois}/{nsemaine}/{jour}/video/new", name="video_new", methods={"GET","POST"}, requirements={"annee":"\d+", "mois":"\d+", "semaine":"\d+", "jour":"\d+"})
     */
    public function videoNew(Request $request, int $annee, int $mois, int $nsemaine, int $jour, CalendrierRepository $calendrierRepository, EnseignantRepository $enseignantRepository): Response
    {
        if ($this->getUser() and in_array("ROLE_ENSEIGNANT", $this->getUser()->getRoles())) {
            $video = new Video();
            $form = $this->createForm(VideoFormType::class, $video);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $classe = $enseignantRepository->find($this->getUser()->getIdCible())->getClasse();
                $calendrier = $calendrierRepository->findCalendrierJourByClasse($jour, $mois, $annee, $classe);
                if ($calendrier == null) {
                    $calendrier = new Calendrier();
                    $calendrier->setJours($jour)
                        ->setMois($mois)
                        ->setAnnee($annee)
                        ->setClasse($classe)
                        ->setSemaine($nsemaine)
                        ->setDescription("Rien à faire pour aujourd'hui");
                }
                $calendrier->addVideo($video);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($video);
                $entityManager->persist($calendrier);
                $entityManager->flush();
                // do anything else you need here, like send an email

                return $this->redirectToRoute('calendrier', ['annee' => $annee, 'mois' => $mois, 'nsemaine' => $nsemaine, 'jour' => $jour]);
            }

            return $this->render('videoNew.html.twig', [
                'videoForm' => $form->createView(),
            ]);
        }else{
            return $this->redirectToRoute('accueil');
        }

    }

    /**
     * @Route("/calendrier/{annee}/{mois}/{nsemaine}/{jour}/document/new", name="document_new", methods={"GET","POST"}, requirements={"annee":"\d+", "mois":"\d+", "semaine":"\d+", "jour":"\d+"})
     */
    public function documentNew(Request $request, SluggerInterface $slugger, int $annee, int $mois, int $nsemaine, int $jour, CalendrierRepository $calendrierRepository, EnseignantRepository $enseignantRepository): Response
    {
        if ($this->getUser() and in_array("ROLE_ENSEIGNANT", $this->getUser()->getRoles())) {
            $document = new Document();
            $form = $this->createForm(DocumentFormType::class, $document);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $documentPDF = $form->get('pdf')->getData();
                if ($documentPDF) {
                    $originalFilename = pathinfo($documentPDF->getClientOriginalName(), PATHINFO_FILENAME);

                    $safeFilename = $slugger->slug($originalFilename);
                    $newFilename = $safeFilename.'-'.uniqid().'.'.$documentPDF->guessExtension();

                    try {
                        $documentPDF->move(
                            $this->getParameter('documents_directory'),
                            $newFilename
                        );
                    } catch (FileException $e) {

                    }

                    $document->setLien($newFilename);
                }
                $classe = $enseignantRepository->find($this->getUser()->getIdCible())->getClasse();
                $calendrier = $calendrierRepository->findCalendrierJourByClasse($jour, $mois, $annee, $classe);
                if ($calendrier==null){
                    $calendrier = new Calendrier();
                    $calendrier->setJours($jour)
                        ->setMois($mois)
                        ->setAnnee($annee)
                        ->setClasse($classe)
                        ->setSemaine($nsemaine)
                        ->setDescription("Rien à faire pour aujourd'hui");
                }
                $calendrier->addDocument($document);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($document);
                $entityManager->persist($calendrier);
                $entityManager->flush();
                // do anything else you need here, like send an email

                return $this->redirectToRoute('calendrier', ['annee' => $annee, 'mois' => $mois, 'nsemaine' => $nsemaine, 'jour' => $jour]);
            }

            return $this->render('documentNew.html.twig', [
                'documentForm' => $form->createView(),
            ]);
        }else{
            return $this->redirectToRoute('accueil');
        }
    }

    /**
     * @Route("/calendrier/{annee}/{mois}/{nsemaine}/{jour}/jeu/new", name="jeu_new", methods={"GET","POST"}, requirements={"annee":"\d+", "mois":"\d+", "semaine":"\d+", "jour":"\d+"})
     */
    public function jeuNew(Request $request, int $annee, int $mois, int $nsemaine, int $jour, CalendrierRepository $calendrierRepository, EnseignantRepository $enseignantRepository): Response
    {
        if ($this->getUser() and in_array("ROLE_ENSEIGNANT", $this->getUser()->getRoles())) {
            $jeu = new Jeu();
            $form = $this->createForm(JeuFormType::class, $jeu);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $classe = $enseignantRepository->find($this->getUser()->getIdCible())->getClasse();
                $calendrier = $calendrierRepository->findCalendrierJourByClasse($jour, $mois, $annee, $classe);
                if ($calendrier==null){
                    $calendrier = new Calendrier();
                    $calendrier->setJours($jour)
                        ->setMois($mois)
                        ->setAnnee($annee)
                        ->setClasse($classe)
                        ->setSemaine($nsemaine)
                        ->setDescription("Rien à faire pour aujourd'hui");
                }
                $calendrier->addJeu($jeu);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($jeu);
                $entityManager->persist($calendrier);
                $entityManager->flush();
                // do anything else you need here, like send an email

                return $this->redirectToRoute('calendrier', ['annee' => $annee, 'mois' => $mois, 'nsemaine' => $nsemaine, 'jour' => $jour]);
            }

            return $this->render('jeuNew.html.twig', [
                'jeuForm' => $form->createView(),
            ]);
        }else{
            return $this->redirectToRoute('accueil');
        }
    }

    /**
     * @Route("/calendrier/{annee}/{mois}/{nsemaine}/{jour}/jeu/{id}/up", name="jeu_up", methods={"GET","POST"}, requirements={"annee":"\d+", "mois":"\d+", "semaine":"\d+", "jour":"\d+", "id":"\d+"})
     */
    public function jeuUp(Request $request, int $annee, int $mois, int $nsemaine, int $jour, Jeu $jeu): Response
    {
        if ($this->getUser() and in_array("ROLE_ENSEIGNANT", $this->getUser()->getRoles())) {
            $form = $this->createForm(JeuFormType::class, $jeu);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($jeu);
                $entityManager->flush();

                return $this->redirectToRoute('calendrier', ['annee' => $annee, 'mois' => $mois, 'nsemaine' => $nsemaine, 'jour' => $jour]);
            }

            return $this->render('jeuNew.html.twig', [
                'jeuForm' => $form->createView(),
            ]);
        }else{
            return $this->redirectToRoute('accueil');
        }
    }

    /**
     * @Route("/calendrier/{annee}/{mois}/{nsemaine}/{jour}/video/{id}/up", name="video_up", methods={"GET","POST"}, requirements={"annee":"\d+", "mois":"\d+", "semaine":"\d+", "jour":"\d+", "id":"\d+"})
     */
    public function videoUp(Request $request, int $annee, int $mois, int $nsemaine, int $jour, Video $video): Response
    {
        if ($this->getUser() and in_array("ROLE_ENSEIGNANT", $this->getUser()->getRoles())) {
            $form = $this->createForm(VideoFormType::class, $video);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($video);
                $entityManager->flush();

                return $this->redirectToRoute('calendrier', ['annee' => $annee, 'mois' => $mois, 'nsemaine' => $nsemaine, 'jour' => $jour]);
            }

            return $this->render('jeuNew.html.twig', [
                'jeuForm' => $form->createView(),
            ]);
        }else{
            return $this->redirectToRoute('accueil');
        }
    }

    /**
     * @Route("/calendrier/{annee}/{mois}/{nsemaine}/{jour}/document/{id}/up", name="document_up", methods={"GET","POST"}, requirements={"annee":"\d+", "mois":"\d+", "semaine":"\d+", "jour":"\d+", "id":"\d+"})
     */
    public function documentUp(Request $request, int $annee, int $mois, int $nsemaine, int $jour, Document $document): Response
    {
        if ($this->getUser() and in_array("ROLE_ENSEIGNANT", $this->getUser()->getRoles())) {
            $form = $this->createForm(DocumentFormType::class, $document);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($document);
                $entityManager->flush();

                return $this->redirectToRoute('calendrier', ['annee' => $annee, 'mois' => $mois, 'nsemaine' => $nsemaine, 'jour' => $jour]);
            }

            return $this->render('documentNew.html.twig', [
                'documentForm' => $form->createView(),
            ]);
        }else{
            return $this->redirectToRoute('accueil');
        }
    }

    /**
     * @Route("/memory", name="memoire")
     */
    public function memoire(): Response
    {
        return $this->render('memory.html.twig', [
        ]);

    }

    /**
     * @Route("/classe/{id}/eleves/", name="liste_eleve", requirements={"id":"\d+"})
     */
    public function listeEleve(Classe $classe): Response
    {
        if ($this->getUser()) {
            return $this->render('listeEleves.html.twig', [
                'classe' => $classe,
            ]);
        } else {
            return $this->redirectToRoute("accueil");
        }

    }

    /**
     * @Route("/utilisateurs", name="liste_utilisateur")
     */
    public function listeUtilisateur(UtilisateurRepository $utilisateurRepository, EleveRepository $eleveRepository, EnseignantRepository $enseignantRepository): Response
    {
        if ($this->getUser() and in_array('ROLE_ADMIN', $this->getUser()->getRoles())) {
            $listeUtilisateur = $utilisateurRepository->findAll();
            $listeEleve = [];
            $listeEnseignant = [];
            $listeAdmin = [];
            foreach ($listeUtilisateur as $utilisateur) {
                if (in_array("ROLE_ELEVE", $utilisateur->getRoles())) {
                    $listeEleve[] = $eleveRepository->find($utilisateur->getIdCible());
                }
                if (in_array("ROLE_ENSEIGNANT", $utilisateur->getRoles())) {
                    $listeEnseignant[] = $enseignantRepository->find($utilisateur->getIdCible());
                }
                if (in_array("ROLE_ADMIN", $utilisateur->getRoles())) {
                    $listeAdmin[] = $utilisateur;
                }
            }
            return $this->render('listeUtilisateurs.html.twig', [
                'eleves' => $listeEleve,
                'enseignants' => $listeEnseignant,
                'admins' => $listeAdmin,
            ]);
        } else {
            return $this->redirectToRoute("accueil");
        }

    }

    /**
     * @Route("/utilisateur/delete/{id}", name="verif_suppr_ut", requirements={"id":"\d+"})
     */
    public function verifSupprimerUt(Utilisateur $utilisateur, EleveRepository $eleveRepository, EnseignantRepository $enseignantRepository): Response
    {
        if (!$this->getUser() or !in_array('ROLE_ADMIN', $this->getUser()->getRoles())){
            return $this->redirectToRoute("accueil");
        }
        $compte = null;
        if (in_array('ROLE_ELEVE', $utilisateur->getRoles())){
            $compte = $eleveRepository->find($utilisateur->getIdCible());
        }
        if (in_array('ROLE_ENSEIGNANT', $utilisateur->getRoles())){
            $compte = $enseignantRepository->find($utilisateur->getIdCible());
        }
        return $this->render('verifSuppr.html.twig', [
            'utilisateur' => $utilisateur,
            'compte' => $compte,
        ]);
    }

    /**
     * @Route("utilisateur/{id}/delete", name="utilisateur_delete", requirements={"id":"\d+"})
     */
    public function delete(Request $request, Utilisateur $utilisateur, EleveRepository $eleveRepository, EnseignantRepository $enseignantRepository): Response
    {$user = $this->getUser();
        if (!$user or !in_array('ROLE_ADMIN', $user->getRoles())){
            return $this->redirectToRoute('accueil');
        }
        $compte = null;
        $classe = null;
        if (in_array('ROLE_ELEVE', $utilisateur->getRoles())){
            $compte = $eleveRepository->find($utilisateur->getIdCible());
            $classe = $compte->getClasse();
            $classe->removeElefe($compte);
        }
        if (in_array('ROLE_ENSEIGNANT', $utilisateur->getRoles())){
            $compte = $enseignantRepository->find($utilisateur->getIdCible());
            $classe = $compte->getClasse();
            $classe->setEnseignant(null);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($classe);
        $entityManager->remove($compte);
        $entityManager->remove($utilisateur);
        $entityManager->flush();

        return $this->redirectToRoute('liste_utilisateur');
    }

    /**
     * @Route("/eleve/{id}/up", name="eleve_up", methods={"GET","POST"}, requirements={"id":"\d+"})
     */
    public function eleveUp(Request $request, Utilisateur $utilisateur, EleveRepository $eleveRepository, ClasseRepository $classeRepository): Response
    {
        if ($this->getUser() and in_array("ROLE_ADMIN", $this->getUser()->getRoles())) {
            $eleve = $eleveRepository->find($utilisateur->getIdCible());
            $classe = $eleve->getClasse();
            $form = $this->createForm(EleveEditFormType::class, $utilisateur);
            $form->handleRequest($request);


            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $classeNew = $classeRepository->find($form->get("classe")->getData());
                $classe->removeElefe($eleve);
                $eleve->setNom($form->get("nom")->getData())
                    ->setPrenom($form->get("prenom")->getData())
                    ->setMail($form->get("mail")->getData())
                    ->setClasse($classeNew);
                $classeNew->addElefe($eleve);
                $entityManager->persist($classe);
                $entityManager->persist($classeNew);
                $entityManager->persist($eleve);
                $entityManager->persist($utilisateur);
                $entityManager->flush();

                return $this->redirectToRoute('info_eleve', ['id' => $utilisateur->getIdCible()]);
            }
            if(!$form->isSubmitted()) {
                $form->get("nom")->setData($eleve->getNom());
                $form->get("prenom")->setData($eleve->getPrenom());
                $form->get("mail")->setData($eleve->getMail());
                $form->get("classe")->setData($eleve->getClasse()->getId());
            }

            return $this->render('eleveEdit.html.twig', [
                'eleveForm' => $form->createView(),
                'eleve' => $eleve,
            ]);
        }else{
            return $this->redirectToRoute('accueil');
        }
    }

    /**
     * @Route("/enseignant/{id}/up", name="enseignant_up", methods={"GET","POST"}, requirements={"id":"\d+"})
     */
    public function enseignantUp(Request $request, Utilisateur $utilisateur, EnseignantRepository $enseignantRepository, ClasseRepository $classeRepository): Response
    {
        if ($this->getUser() and in_array("ROLE_ADMIN", $this->getUser()->getRoles())) {
            $enseignant = $enseignantRepository->find($utilisateur->getIdCible());
            $classe = $enseignant->getClasse();
            $changementEns = false;
            $form = $this->createForm(EnseignantEditFormType::class, $utilisateur);
            $form->handleRequest($request);


            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $classeId = $form->get("classe")->getData();
                if ($classeId!=0) {
                    $classeNew = $classeRepository->find($classeId);
                    $enseignantAnc = $classeNew->getEnseignant();
                } else {
                    $classeNew = null;
                    $enseignantAnc = null;
                }

                if ($enseignantAnc){
                    $changementEns = true;
                }
                $enseignant->setNom($form->get("nom")->getData())
                    ->setPrenom($form->get("prenom")->getData())
                    ->setMail($form->get("mail")->getData());
                if (!$changementEns){
                    $enseignant->setClasse($classeNew);
                }
                if ($classeNew and !$changementEns){
                    $classeNew->setEnseignant($enseignant);
                    $entityManager->persist($classeNew);
                }
                $entityManager->flush();
                $entityManager->persist($enseignant);
                $entityManager->persist($utilisateur);
                $entityManager->flush();

                if ($changementEns){
                    return $this->redirectToRoute('changement_classe', ['id' => $enseignant->getId(), 'idClasse' => $classeNew->getId()]);
                }

                return $this->redirectToRoute('info_enseignant', ['id' => $utilisateur->getIdCible()]);
            }
            if (!$form->isSubmitted()) {
                $form->get("nom")->setData($enseignant->getNom());
                $form->get("prenom")->setData($enseignant->getPrenom());
                $form->get("mail")->setData($enseignant->getMail());
                if ($enseignant->getClasse()) {
                    $form->get("classe")->setData($enseignant->getClasse()->getId());
                } else {
                    $form->get("classe")->setData(0);
                }
            }


            return $this->render('enseignantEdit.html.twig', [
                'enseignantForm' => $form->createView(),
                'enseignant' => $enseignant,
            ]);
        }else{
            return $this->redirectToRoute('accueil');
        }
    }
}
