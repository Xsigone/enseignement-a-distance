<?php

namespace App\Entity;

use App\Repository\MessageRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MessageRepository::class)
 */
class Message
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $message;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateur::class, inversedBy="messagesSend")
     */
    private $utilisateur1;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateur::class, inversedBy="messagesRecv")
     */
    private $utilisateur2;

    /**
     * @ORM\Column(type="boolean")
     */
    private $lu;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(string $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getUtilisateur1(): ?Utilisateur
    {
        return $this->utilisateur1;
    }

    public function setUtilisateur1(?Utilisateur $utilisateur1): self
    {
        $this->utilisateur1 = $utilisateur1;

        return $this;
    }

    public function getUtilisateur2(): ?Utilisateur
    {
        return $this->utilisateur2;
    }

    public function setUtilisateur2(?Utilisateur $utilisateur2): self
    {
        $this->utilisateur2 = $utilisateur2;

        return $this;
    }

    public function getLu(): ?bool
    {
        return $this->lu;
    }

    public function setLu(bool $lu): self
    {
        $this->lu = $lu;

        return $this;
    }
}
