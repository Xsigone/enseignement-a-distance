const searchInput = document.getElementById("search");
const rows = document.querySelectorAll("tbody tr");
if (searchInput){
    searchInput.addEventListener("keyup", function (event) {
        const q = event.target.value.toLowerCase();
        rows.forEach((row) => {
            row.querySelector("td").textContent.toLowerCase().includes(q)
                ? (row.style.display = "table-row")
                : (row.style.display = "none");
        });
    });
}