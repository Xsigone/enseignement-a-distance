<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210621181115 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE calendrier (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, classe_id INTEGER DEFAULT NULL, annee INTEGER NOT NULL, mois INTEGER NOT NULL, jours INTEGER NOT NULL, description VARCHAR(255) NOT NULL, semaine INTEGER NOT NULL)');
        $this->addSql('CREATE INDEX IDX_B2753CB98F5EA509 ON calendrier (classe_id)');
        $this->addSql('CREATE TABLE calendrier_video (calendrier_id INTEGER NOT NULL, video_id INTEGER NOT NULL, PRIMARY KEY(calendrier_id, video_id))');
        $this->addSql('CREATE INDEX IDX_35D05B5AFF52FC51 ON calendrier_video (calendrier_id)');
        $this->addSql('CREATE INDEX IDX_35D05B5A29C1004E ON calendrier_video (video_id)');
        $this->addSql('CREATE TABLE calendrier_document (calendrier_id INTEGER NOT NULL, document_id INTEGER NOT NULL, PRIMARY KEY(calendrier_id, document_id))');
        $this->addSql('CREATE INDEX IDX_295EB210FF52FC51 ON calendrier_document (calendrier_id)');
        $this->addSql('CREATE INDEX IDX_295EB210C33F7837 ON calendrier_document (document_id)');
        $this->addSql('CREATE TABLE calendrier_jeu (calendrier_id INTEGER NOT NULL, jeu_id INTEGER NOT NULL, PRIMARY KEY(calendrier_id, jeu_id))');
        $this->addSql('CREATE INDEX IDX_F5A3D6B8FF52FC51 ON calendrier_jeu (calendrier_id)');
        $this->addSql('CREATE INDEX IDX_F5A3D6B88C9E392E ON calendrier_jeu (jeu_id)');
        $this->addSql('CREATE TABLE classe (id INTEGER NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE document (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, lien VARCHAR(255) DEFAULT NULL)');
        $this->addSql('CREATE TABLE eleve (id INTEGER NOT NULL, classe_id INTEGER DEFAULT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, mail VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_ECA105F78F5EA509 ON eleve (classe_id)');
        $this->addSql('CREATE TABLE enseignant (id INTEGER NOT NULL, classe_id INTEGER DEFAULT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, mail VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_81A72FA18F5EA509 ON enseignant (classe_id)');
        $this->addSql('CREATE TABLE jeu (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, lien VARCHAR(255) DEFAULT NULL)');
        $this->addSql('CREATE TABLE message (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, utilisateur1_id INTEGER DEFAULT NULL, utilisateur2_id INTEGER DEFAULT NULL, message VARCHAR(255) NOT NULL, date VARCHAR(255) NOT NULL, lu BOOLEAN NOT NULL)');
        $this->addSql('CREATE INDEX IDX_B6BD307F30F4F973 ON message (utilisateur1_id)');
        $this->addSql('CREATE INDEX IDX_B6BD307F2241569D ON message (utilisateur2_id)');
        $this->addSql('CREATE TABLE utilisateur (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, identifiant VARCHAR(180) NOT NULL, roles CLOB NOT NULL --(DC2Type:json)
        , password VARCHAR(255) NOT NULL, id_cible INTEGER DEFAULT NULL)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1D1C63B3C90409EC ON utilisateur (identifiant)');
        $this->addSql('CREATE TABLE video (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, lien VARCHAR(255) DEFAULT NULL)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE calendrier');
        $this->addSql('DROP TABLE calendrier_video');
        $this->addSql('DROP TABLE calendrier_document');
        $this->addSql('DROP TABLE calendrier_jeu');
        $this->addSql('DROP TABLE classe');
        $this->addSql('DROP TABLE document');
        $this->addSql('DROP TABLE eleve');
        $this->addSql('DROP TABLE enseignant');
        $this->addSql('DROP TABLE jeu');
        $this->addSql('DROP TABLE message');
        $this->addSql('DROP TABLE utilisateur');
        $this->addSql('DROP TABLE video');
    }
}
